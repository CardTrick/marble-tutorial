﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	// Call this to end the game and restart the level.
	public void GameOver() {
		Application.LoadLevel(Application.loadedLevel);
	}

	// Call this to do the exact same thing. You can implement your own LevelComplete function if you wish.
	public void LevelComplete() {
		Application.LoadLevel(Application.loadedLevel);
	}
}
