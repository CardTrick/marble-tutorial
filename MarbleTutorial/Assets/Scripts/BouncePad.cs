﻿using UnityEngine;
using System.Collections;

public class BouncePad : MonoBehaviour {

	public float strength = 10.0f;

	/* When the player enters the trigger zone, launch them into the air. */
	void OnTriggerEnter(Collider other) {
		// Check if 'other' is the player.
		if (other.gameObject.tag == "Player") {
			/* Add some vertical force. We use -transform.forward because the BouncePad is 
			 * on its front, so to speak. Negative-forwards for this transform is up. */
			other.rigidbody.AddForce(-transform.forward * strength, ForceMode.VelocityChange);
		}
	}
}
