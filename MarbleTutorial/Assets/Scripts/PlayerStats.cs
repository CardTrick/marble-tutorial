﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	public int score = 0;

	private TextMesh scoreCount;
	private TextMesh scoreCountShadow;

	void Start() {
		// Store a reference to the two TextMesh's holding the score text.
		scoreCount = transform.Find("ScoreCount").GetComponent<TextMesh>();
		scoreCountShadow = transform.Find("ScoreCountShadow").GetComponent<TextMesh>();
	}

	/* Increase the score and update the text on the HUD. */
	public void AddScore(int amount) {
		score += amount;
		scoreCount.text = score.ToString();
		scoreCountShadow.text = score.ToString();
	}
}
