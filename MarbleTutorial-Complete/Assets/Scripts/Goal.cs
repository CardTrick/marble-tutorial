﻿using UnityEngine;
using System.Collections;


public class Goal: MonoBehaviour {

	private Transform sensor;


	void Start() {
		// Store the sensor so we can use it later.
		sensor = transform.Find("GoalSensor");
	}

	/* Here we should Raycast from the sensor to detect if the player is breaking the beam. */
	void FixedUpdate() {
		bool playerBetweenPosts = Physics.Raycast(sensor.position, sensor.forward, 1.5f, (1 << 8));
		if (playerBetweenPosts) {
			GameObject gameController = GameObject.FindWithTag("GameController");
			gameController.GetComponent<GameController>().LevelComplete();
		}
	}
}
