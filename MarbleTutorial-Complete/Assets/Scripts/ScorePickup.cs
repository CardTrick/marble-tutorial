﻿using UnityEngine;
using System.Collections;

public class ScorePickup : MonoBehaviour {

	public int score = 1;
	public float spinSpeed = 10.0f;


	void FixedUpdate() {
		// This will make the object rotate slowly around the Y axis.
		transform.Rotate(0, spinSpeed * Time.deltaTime, 0);
	}

	/* If the player enters the trigger zone, give the player some points and destroy this Pickup. */
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			PlayerStats stats = GameObject.FindWithTag("PlayerStats").GetComponent<PlayerStats>();
			stats.AddScore(score);
			Destroy(gameObject);
		}
	}
}
