﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {
	
	public Vector3 displacement = Vector3.zero;
	public float speed = 10.0f;

	private Vector3 startPoint;
	private Vector3 endPoint;
	private float time = 0.0f;
	
	void Start () {
		// Store the start and end points.
		startPoint = transform.position;
		endPoint = startPoint + displacement;
	}

	/* Here we want to move the platform smoothly between the start and end points. */
	void FixedUpdate () {
		time += Time.deltaTime;
		float prop = (Mathf.Sin(time*speed)+1)/2;
		rigidbody.MovePosition(Vector3.Lerp(startPoint, endPoint, prop));
	}
}
