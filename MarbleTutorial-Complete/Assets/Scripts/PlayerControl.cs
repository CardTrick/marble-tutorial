﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public float acceleration = 100.0f;
	public float jumpPower = 5.0f;


	// This happens when you start the game.
	void Start () {
		// Unity puts a limit on Angular Velocity by default. Our player needs a higher maximum.
		rigidbody.maxAngularVelocity = 20.0f;
	}
	
	/* This happens once every physics step. */
	void FixedUpdate() {
		float horizontalMotion = -Input.GetAxis("Horizontal");
		float verticalMotion = Input.GetAxis("Vertical");
		Accelerate(verticalMotion, horizontalMotion);
		// If the jump button is pressed, and the Player is touching the ground: Jump!
		if (Input.GetButton("Jump") && IsGrounded()) {
			Jump();
		}
	}

	/* Here is where we want to give the Player some torque so they can roll. */
	void Accelerate(float forward, float sideways) {
		Vector3 torqueVector = new Vector3(forward, 0, sideways);
		rigidbody.AddTorque(torqueVector * acceleration, ForceMode.Acceleration);
	}

	/* Here is where we want to check if the Player is touching the ground. */
	bool IsGrounded() {
		int notPlayerMask = ~(1 << 8);
		Vector3 aBitBelow = -Vector3.up*0.05f;
		bool onGround = Physics.CheckSphere(transform.position+aBitBelow, 0.5f, notPlayerMask);
		return onGround;
	}

	/* Here is where we want to give the player some vertical velocity so they jump. */
	void Jump() {
		Vector3 newVelocity = rigidbody.velocity;
		newVelocity.y = jumpPower;
		rigidbody.velocity = newVelocity;
	}
}
